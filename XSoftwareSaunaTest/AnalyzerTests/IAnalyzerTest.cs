﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SoftwareSauna.Models.Analyzers;
using SoftwareSauna.Models.Analyzers.Interfaces;
using SoftwareSauna.Models.Helpers.Interfaces;
using Moq;

namespace XSoftwareSaunaTest.AnalyzerTests
{
    public class IAnalyzerTest
    {
        #region ValidatePath
        public static IEnumerable<object[]> IAnalyzerTest_ValidatePath_ShouldReturnTrue_data =>
        new List<object[]>
        {
            new object[] { new DownAnalyzer(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 0, 2, "",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(), 
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new DownAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 0, 2, "|A+",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new DownAnalyzer(),new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 0, 2, "",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},

            new object[] { new UpAnalyzer(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 0, 2, "",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new UpAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 3, 9, "|+",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new UpAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 6, 11, "|x",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new UpAnalyzer(), new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 8, 9, "|C|||A",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},

            new object[] { new LeftAnalyzer(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 2, 6, "-B-x",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new LeftAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 0, 2, "",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new LeftAnalyzer(), new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 2, 9, "--|-----K",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},

            new object[] { new RightAnalyzer(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 0, 2, "---A---+",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new RightAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 0, 2, "",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new RightAnalyzer(), new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 6, 0, "--E--Ex",
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)}
        };

        [Theory]
        [MemberData(nameof(IAnalyzerTest_ValidatePath_ShouldReturnTrue_data))]
        public void IAnalyzerTest_ValidatePath_ShouldReturnTrue(IAnalyzer analyzer, List<string> lines, int rowIndex, int columnIndex, string expectedPath, 
                                                                IParseHelper helper)
        {
            StringBuilder path = new StringBuilder();
            analyzer.StartAnalyzing(lines, rowIndex, columnIndex, path, new StringBuilder(), new List<Tuple<int, int>>(), helper);

            Assert.Equal(expectedPath, path.ToString());
        }
        #endregion

        #region ValidateIndexes
        public static IEnumerable<object[]> IAnalyzerTest_ValidateIndexes_ShouldReturnTrue_data =>
        new List<object[]>
        {
            new object[] { new DownAnalyzer(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 0, 2, null,
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new DownAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 0, 2, (3, 2),
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new DownAnalyzer(),new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 0, 2, null,
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},

            new object[] { new UpAnalyzer(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 0, 2, null,
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new UpAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 3, 9, (1,9),
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new UpAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 6, 11, (4,11),
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new UpAnalyzer(), new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 8, 9, (2,9),
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},

            new object[] { new LeftAnalyzer(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 2, 6, (2, 2),
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new LeftAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 0, 2, null,
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new LeftAnalyzer(), new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 2, 9, (2,0),
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},

            new object[] { new RightAnalyzer(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 0, 2, (0, 10),
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new RightAnalyzer(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 0, 2, null,
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)},
            new object[] { new RightAnalyzer(), new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 6, 0, (6,7),
                                                                  Mock.Of<IParseHelper>(x=> x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(),
                                                                  It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>()) == true)}
        };

        [Theory]
        [MemberData(nameof(IAnalyzerTest_ValidateIndexes_ShouldReturnTrue_data))]
        public void IAnalyzerTest_ValidateIndexes_ShouldReturnTrue(IAnalyzer analyzer, List<string> lines, int rowIndex, int columnIndex,
                                                                   (int RowIndex, int ColumnIndex)? expectedResult, IParseHelper helper)
        {
            var result = analyzer.StartAnalyzing(lines, rowIndex, columnIndex, new StringBuilder(), new StringBuilder(), new List<Tuple<int, int>>(), helper);

            Assert.Equal(expectedResult, result);
        }
        #endregion
    }
}
