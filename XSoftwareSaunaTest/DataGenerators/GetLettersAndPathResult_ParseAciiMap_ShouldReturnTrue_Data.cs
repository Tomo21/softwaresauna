﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using SoftwareSauna.Models.Helpers.Interfaces;
using SoftwareSauna.Models.Code;
using SoftwareSauna.Models.Code.Interfaces;
using Moq;
using SoftwareSauna.Models.Analyzers;
using SoftwareSauna.Models.Analyzers.Interfaces;
using SoftwareSauna.Models;

namespace XSoftwareSaunaTest.DataGenerators
{
    public class GetLettersAndPathResult_ParseAciiMap_ShouldReturnTrue_Data : IEnumerable<object[]>
    {
        enum TestCases{ FirstCase, SecondCase, ThirdCase }

        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[] { asciiMaps[(int)TestCases.FirstCase], expectedResults[(int)TestCases.FirstCase],
                GetParseAsciiMap(TestCases.FirstCase, 0, 2)};

            yield return new object[] { asciiMaps[(int)TestCases.SecondCase], expectedResults[(int)TestCases.SecondCase],
                GetParseAsciiMap(TestCases.SecondCase, 0, 2)};

            yield return new object[] { asciiMaps[(int)TestCases.ThirdCase], expectedResults[(int)TestCases.ThirdCase] ,
                GetParseAsciiMap(TestCases.ThirdCase, 0, 2)};
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        #region String Data
        private string[] asciiMaps = { "  @---A---+\r\n          |\r\n  x-B-+   C\r\n      |   |\r\n      +---+",//First test
                                       "  @\r\n  | C----+\r\n  A |    |\r\n  +---B--+\r\n    |      x\r\n    |      |\r\n    +---D--+",//Second Test
                                       "  @---+\r\n      B\r\nK-----|--A\r\n|     |  |\r\n|  +--E  |\r\n|  |     |\r\n+--E--Ex C\r\n   |     |\r\n   +--F--+"};//Third Test

        private ResultsViewModel[] expectedResults = { new ResultsViewModel("@---A---+|C|+---+|+-B-x", "ACB"),//First test
                                                       new ResultsViewModel("@|A+---B--+|+----C|-||+---D--+|x", "ABCD"),//Second Test
                                                       new ResultsViewModel("@---+B||E--+|E|+--F--+|C|||A--|-----K|||+--E--Ex", "BEEFCAKE")};//Third Test

        private List<string>[] asciiMapLines = { new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" },//First test
                                                 new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" },//Second Test
                                                 new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" } };//Third Test

        List<Tuple<string, string>>[] resultSegments =  {
            new List<Tuple<string, string>>() { new Tuple<string, string>("---A---+", "A"),//First test
                                                new Tuple<string, string>("|C|+", "C"),
                                                new Tuple<string, string>("---+", ""),
                                                new Tuple<string, string>("|+", ""),
                                                new Tuple<string, string>("-B-x", "Bx")},

            new List<Tuple<string, string>>() { new Tuple<string, string>("|A+", "A"),//Second Test
                                                new Tuple<string, string>("---B--+", "B"),
                                                new Tuple<string, string>("|+----C", "C"),
                                                new Tuple<string, string>("|-||+", ""),
                                                new Tuple<string, string>("---D--+", "D"),
                                                new Tuple<string, string>("|x", "x")},

            new List<Tuple<string, string>>() { new Tuple<string, string>("---+", ""),//Third Test
                                                new Tuple<string, string>("B||E", "BE"),
                                                new Tuple<string, string>("--+", ""),
                                                new Tuple<string, string>("|E|+", "E"),
                                                new Tuple<string, string>("--F--+", "F"),
                                                new Tuple<string, string>("|C|||A", "CA"),
                                                new Tuple<string, string>("--|-----K", "K"),
                                                new Tuple<string, string>("|||+", ""),
                                                new Tuple<string, string>("--E--Ex", "Ex")}
        };                                         
        #endregion

        private IParseHelper GetHelperMock(string asciimap, List<string> lines, int rowIndex, int columnIndex){
            var helper = new Mock<IParseHelper>();
            helper.Setup(x => x.SeparateLines(asciimap)).Returns(lines);
            helper.Setup(x => x.FindStartingCharPosition(It.IsAny<List<string>>())).Returns((rowIndex, columnIndex));
            //helper.Setup(x => x.CheckCharacterAndProcessIt(It.IsAny<char>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>())).Returns(true);

            return helper.Object;
        }

        private AnalyzerFactory GetAnalyzerFactoryMock(TestCases test, int rowIndex, int columnIndex){
            List<string> lines = asciiMapLines[(int)test];
            var analyzer = new Mock<IAnalyzer>();
            
            List<Tuple<string, string>> resultSegmentsPart = resultSegments[(int)test];

            foreach (Tuple<string, string> item in resultSegmentsPart)
            {
                analyzer.Setup(x => x.StartAnalyzing(lines, rowIndex, columnIndex, It.IsAny<StringBuilder>(), It.IsAny<StringBuilder>(), It.IsAny<List<Tuple<int, int>>>(), It.IsAny<IParseHelper>()))
                .Callback((List<string> lines1, int rowIndex1, int columnIndex1, StringBuilder path1, StringBuilder characters1, List<Tuple<int, int>> usedLettersCords1, IParseHelper helper1) =>
                { path1.Append(item.Item1); characters1.Append(item.Item2); })
                .Returns((rowIndex, columnIndex += 1));
            }

            var analyzerFactory = new Mock<AnalyzerFactory>();
            analyzerFactory.Setup(x => x.ExecuteCreation(It.IsAny<AnalyzerFactoryType>())).Returns(analyzer.Object);
        
            return analyzerFactory.Object;
        }

        private IParseAsciiMap GetParseAsciiMap(TestCases test, int rowIndex, int columnIndex){
            var helper = GetHelperMock(asciiMaps[(int) test], asciiMapLines[(int)test], rowIndex, columnIndex);
            var analyzerFactory = GetAnalyzerFactoryMock(test, rowIndex, columnIndex);
            var parseAciiMap = new ParseAsciiMap(helper, analyzerFactory);

            return parseAciiMap;
        }
    }
}