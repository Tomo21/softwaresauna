﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SoftwareSauna.Models.Code.Interfaces;
using XSoftwareSaunaTest.DataGenerators;
using SoftwareSauna.Models;

namespace XSoftwareSaunaTest
{
    public class ParseAsciiMapTest
    {
        [Theory]
        [ClassData(typeof(GetLettersAndPathResult_ParseAciiMap_ShouldReturnTrue_Data))]
        public void GetLettersAndPathResult_ValidatePath_ShouldReturnTrue(string asciimap, ResultsViewModel expectedResult, IParseAsciiMap parseAciiMap)
        {
            var result = parseAciiMap.GetLettersAndPathResult(asciimap);

            Assert.Equal(expectedResult.AsciiPath, result.AsciiPath);
        }

        [Theory]
        [ClassData(typeof(GetLettersAndPathResult_ParseAciiMap_ShouldReturnTrue_Data))]
        public void GetLettersAndPathResult_ValidateLetters_ShouldReturnTrue(string asciimap, ResultsViewModel expectedResult, IParseAsciiMap parseAciiMap)
        {
            var result = parseAciiMap.GetLettersAndPathResult(asciimap);

            Assert.Equal(expectedResult.Letters, result.Letters);
        }

    }

}

