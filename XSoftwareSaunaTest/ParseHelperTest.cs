using System;
using Xunit;
using SoftwareSauna.Models.Helpers;
using SoftwareSauna.Models.Helpers.Interfaces;
using System.Collections.Generic;
using System.Text;

namespace XSoftwareSaunaTest
{
    public class ParseHelperTest
    {
        #region SeparateLines
        public static IEnumerable<object[]> SeparateLines_SplitAsciiMapStringToList_ShouldReturnTrue_Data =>
        new List<object[]>
        {
            new object[] { new ParseHelper(), "  @---A---+\r\n          |\r\n  x-B-+   C\r\n      |   |\r\n      +---+", new List<string>() { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" } }       
        };

        [Theory]
        [MemberData(nameof(SeparateLines_SplitAsciiMapStringToList_ShouldReturnTrue_Data))]
        public void SeparateLines_SplitAsciiMapStringToList_ShouldReturnTrue(IParseHelper helper, string asciiMap, List<string> expectedResult)
        {
            var result = helper.SeparateLines(asciiMap);
            Assert.Equal(expectedResult, result);
        }
        #endregion

        #region FindStartingCharColumnIndex
        public static IEnumerable<object[]> FindStartingCharPosition_FindColumnIndex_ShouldReturnTrue_Data =>
        new List<object[]>
        {
            new object[] { new ParseHelper(), new List<string> { "  @---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, 2 },
            new object[] { new ParseHelper(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 2 },
            new object[] { new ParseHelper(), new List<string> { "  @---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "+--E--Ex C", "   |     |", "   +--F--+" }, 2 },
            new object[] { new ParseHelper(), new List<string> { "  ---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, -1 }
        };

        [Theory]
        [MemberData(nameof(FindStartingCharPosition_FindColumnIndex_ShouldReturnTrue_Data))]
        public void FindStartingCharPosition_FindColumnIndex_ShouldReturnTrue(IParseHelper helper, List<string> lines, int expectedColumnIndex)
        {            
            var result = helper.FindStartingCharPosition(lines);

            Assert.Equal(expectedColumnIndex, result.ColumnIndex);
        }

        #endregion

        #region FindStartingCharRowIndex
        public static IEnumerable<object[]> FindStartingCharPosition_FindRowIndex_ShouldReturnTrue_Data =>
        new List<object[]>
        {
            new object[] { new ParseHelper(), new List<string> { "  ---A---+", "          |", "@  x-B-+   C", "      |   |", "      +---+" }, 2 },
            new object[] { new ParseHelper(), new List<string> { "  @", "  | C----+", "  A |    |", "  +---B--+", "    |      x", "    |      |", "    +---D--+" }, 0 },
            new object[] { new ParseHelper(), new List<string> { "  ---+", "      B", "K-----|--A", "|     |  |", "|  +--E  |", "|  |     |", "@+--E--Ex C", "   |     |", "   +--F--+" }, 6 },
            new object[] { new ParseHelper(), new List<string> { "  ---A---+", "          |", "  x-B-+   C", "      |   |", "      +---+" }, -1 }
        };

        [Theory]
        [MemberData(nameof(FindStartingCharPosition_FindRowIndex_ShouldReturnTrue_Data))]
        public void FindStartingCharPosition_FindRowIndex_ShouldReturnTrue(IParseHelper helper, List<string> lines, int expectedRowIndex)
        {
            var result = helper.FindStartingCharPosition(lines);

            Assert.Equal(expectedRowIndex, result.RowIndex);
        }
        #endregion

        #region CheckCharacterAndProcessIt_CheckBoolMethodReturn
        public static IEnumerable<object[]> CheckCharacterAndProcessIt_CheckBoolMethodReturn_data =>
        new List<object[]>
        {
            new object[] { new ParseHelper(), true, "A", 4, 3, new StringBuilder(), new List<Tuple<int, int>>() },
            new object[] { new ParseHelper(), true, "B", 0, 2, new StringBuilder(), new List<Tuple<int, int>>() { new Tuple<int, int>(6, 3) } },
            new object[] { new ParseHelper(), false, "C", 6, 3, new StringBuilder(), new List<Tuple<int, int>>() { new Tuple<int, int>(6, 3) } }
        };

        [Theory]
        [MemberData(nameof(CheckCharacterAndProcessIt_CheckBoolMethodReturn_data))]
        public void CheckCharacterAndProcessIt_CheckBoolMethodReturn(IParseHelper helper, bool expectedResult, char item, int rowIndex, int columnIndex, StringBuilder characters, List<Tuple<int, int>> usedLettersCords){
            bool result = helper.CheckCharacterAndProcessIt(item, rowIndex, columnIndex, characters, usedLettersCords);

            Assert.Equal(expectedResult, result);
        }
        #endregion

        #region CheckCharacterAndProcessIt_CheckAddingCharToStringBuilder
        public static IEnumerable<object[]> CheckCharacterAndProcessIt_CheckAddingCharToStringBuilder_data =>
        new List<object[]>
        {
            new object[] { new ParseHelper(), "A", "A", 4, 3, new StringBuilder(), new List<Tuple<int, int>>() },
            new object[] { new ParseHelper(), "AB", "B", 0, 2, new StringBuilder().Append("A"), new List<Tuple<int, int>>() { new Tuple<int, int>(6, 3) } },
            new object[] { new ParseHelper(), "", "C", 6, 3, new StringBuilder(), new List<Tuple<int, int>>() { new Tuple<int, int>(6, 3) } }
        };

        [Theory]
        [MemberData(nameof(CheckCharacterAndProcessIt_CheckAddingCharToStringBuilder_data))]
        public void CheckCharacterAndProcessIt_CheckAddingCharToStringBuilder(IParseHelper helper, string expectedResult, char item, int rowIndex, int columnIndex, StringBuilder characters, List<Tuple<int, int>> usedLettersCords)
        {
            helper.CheckCharacterAndProcessIt(item, rowIndex, columnIndex, characters, usedLettersCords);

            Assert.Equal(expectedResult, characters.ToString());
        }
        #endregion

        #region CheckCharacterAndProcessIt_CheckAddingCharIndexesToUsedLettersListOdTuples
        public static IEnumerable<object[]> CheckCharacterAndProcessIt_CheckAddingCharIndexesToUsedLettersListOdTuples_data =>
        new List<object[]>
        {
            new object[] { new ParseHelper(), new List<Tuple<int, int>>() { new Tuple<int, int>(4, 3) }, 
                           "A", 4, 3, new StringBuilder(), new List<Tuple<int, int>>() },
            new object[] { new ParseHelper(), new List<Tuple<int, int>>() { new Tuple<int, int>(6, 3), new Tuple<int, int>(0, 2) },
                           "B", 0, 2, new StringBuilder().Append("A"), new List<Tuple<int, int>>() { new Tuple<int, int>(6, 3) } },
            new object[] { new ParseHelper(), new List<Tuple<int, int>>() { new Tuple<int, int>(6, 3) }, 
                           "C", 6, 3, new StringBuilder(), new List<Tuple<int, int>>() { new Tuple<int, int>(6, 3) } }
        };

        [Theory]
        [MemberData(nameof(CheckCharacterAndProcessIt_CheckAddingCharIndexesToUsedLettersListOdTuples_data))]
        public void CheckCharacterAndProcessIt_CheckAddingCharIndexesToUsedLettersListOdTuples(IParseHelper helper, List<Tuple<int, int>> expectedResult, char item, int rowIndex, int columnIndex, StringBuilder characters, List<Tuple<int, int>> usedLettersCords)
        {
            helper.CheckCharacterAndProcessIt(item, rowIndex, columnIndex, characters, usedLettersCords);

            Assert.Equal(expectedResult, usedLettersCords);
        }
        #endregion
    }
}
