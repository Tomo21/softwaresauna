﻿using SoftwareSauna.Models.Analyzers.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareSauna.Models.Analyzers
{
    public enum AnalyzerFactoryType { UpAnalyzer, DownAnalyzer, LeftAnalyzer, RightAnalyzer };

    public class AnalyzerFactory
    {
        private readonly ConcurrentDictionary<AnalyzerFactoryType, IAnalyzerFactory> _factories;

        public AnalyzerFactory(){
            _factories = new ConcurrentDictionary<AnalyzerFactoryType, IAnalyzerFactory>();

            AnalyzerFactoryType[] t = (AnalyzerFactoryType[]) Enum.GetValues(typeof(AnalyzerFactoryType));
            Parallel.ForEach(t.ToList(), (type) => {
                var factory = (IAnalyzerFactory)Activator.CreateInstance(Type.GetType("SoftwareSauna.Models.Analyzers." + Enum.GetName(typeof(AnalyzerFactoryType), type) + "Factory"));
                _factories.TryAdd(type, factory);
            });
        }

        //Moq demands method to be virtual
        public virtual IAnalyzer ExecuteCreation(AnalyzerFactoryType type) => _factories[type].Create();
    }
}
