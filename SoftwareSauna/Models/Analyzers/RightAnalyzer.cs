﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftwareSauna.Models.Analyzers.Interfaces;
using SoftwareSauna.Models.Helpers.Interfaces;

namespace SoftwareSauna.Models.Analyzers
{
    public class RightAnalyzer : IAnalyzer
    {
        public (int RowIndex, int ColumnIndex)? StartAnalyzing(List<string> lines, int rowIndex, int columnIndex, StringBuilder path, StringBuilder characters, List<Tuple<int, int>> usedLettersCords, IParseHelper helper)
        {
            columnIndex += 1;
            int counter = columnIndex;
            for (; counter < lines[rowIndex].Length; counter++)
            {
                char item = lines[rowIndex][counter];

                if (item == ' ')
                    break;

                path.Append(item);
                if (!helper.CheckCharacterAndProcessIt(item, rowIndex, counter, characters, usedLettersCords))
                {
                    continue;
                }
            }

            if (counter == columnIndex)
                return null;

            return (rowIndex, counter - 1);
        }
    }
}
