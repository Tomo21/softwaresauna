﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftwareSauna.Models.Analyzers.Interfaces;
using SoftwareSauna.Models.Helpers.Interfaces;

namespace SoftwareSauna.Models.Analyzers
{
    public class UpAnalyzer : IAnalyzer
    {
        public (int RowIndex, int ColumnIndex)? StartAnalyzing(List<string> lines, int rowIndex, int columnIndex, StringBuilder path, StringBuilder characters, List<Tuple<int, int>> usedLettersCords, IParseHelper helper)
        {
            rowIndex -= 1;
            int counter = rowIndex;
            for (; counter >= 0; counter--)
            {
                if (lines[counter].Length <= columnIndex)
                    break;

                char item = lines[counter][columnIndex];

                if (item == ' ')
                    break;

                path.Append(item);
                if (!helper.CheckCharacterAndProcessIt(item, counter, columnIndex, characters, usedLettersCords))
                {
                    continue;
                }
            }

            if (counter == rowIndex)
                return null;

            return (counter + 1, columnIndex);
        }

    }
}
