﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftwareSauna.Models.Helpers.Interfaces;

namespace SoftwareSauna.Models.Analyzers.Interfaces
{
    public interface IAnalyzer
    {
        (int RowIndex, int ColumnIndex)? StartAnalyzing(List<string> lines, int rowIndex, int columnIndex, StringBuilder path, StringBuilder characters, 
                                                        List<Tuple<int, int>> usedLettersCords, IParseHelper helper);
    }
}
