﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareSauna.Models.Analyzers.Interfaces
{
    public interface IAnalyzerFactory
    {
        IAnalyzer Create();
    }
}
