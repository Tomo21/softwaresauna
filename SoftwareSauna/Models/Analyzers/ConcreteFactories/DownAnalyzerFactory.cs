﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoftwareSauna.Models.Analyzers.Interfaces;

namespace SoftwareSauna.Models.Analyzers
{
    public class DownAnalyzerFactory : IAnalyzerFactory
    {
        public IAnalyzer Create()
        {
            return new DownAnalyzer();
        }
    }
}
