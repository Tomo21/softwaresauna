﻿using SoftwareSauna.Models.Analyzers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareSauna.Models.Analyzers
{
    public class RightAnalyzerFactory : IAnalyzerFactory
    {
        public IAnalyzer Create()
        {
            return new RightAnalyzer();
        }
    }
}
