﻿using SoftwareSauna.Models.Analyzers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareSauna.Models.Analyzers
{
    public class UpAnalyzerFactory : IAnalyzerFactory
    {
        public IAnalyzer Create()
        {
            return new UpAnalyzer();
        }
    }
}
