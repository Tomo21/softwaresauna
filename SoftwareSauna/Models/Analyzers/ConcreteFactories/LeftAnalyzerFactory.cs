﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoftwareSauna.Models.Analyzers.Interfaces;

namespace SoftwareSauna.Models.Analyzers
{
    public class LeftAnalyzerFactory : IAnalyzerFactory
    {
        public IAnalyzer Create()
        {
            return new LeftAnalyzer();
        }
    }
}
