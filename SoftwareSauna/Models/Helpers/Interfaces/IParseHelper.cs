﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftwareSauna.Models.Helpers.Interfaces
{
    public interface IParseHelper
    {
        List<string> SeparateLines(string asciiMap);
        (int RowIndex, int ColumnIndex) FindStartingCharPosition(List<string> lines);
        public bool CheckCharacterAndProcessIt(char item, int rowIndex, int columnIndex, StringBuilder characters, List<Tuple<int, int>> usedLettersCords);
    }
}
