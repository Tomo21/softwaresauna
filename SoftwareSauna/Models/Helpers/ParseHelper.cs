﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftwareSauna.Models.Helpers.Interfaces;

namespace SoftwareSauna.Models.Helpers
{
    public class ParseHelper : IParseHelper
    {
        public List<string> SeparateLines(string asciiMap)
        {
            return asciiMap.Split("\r\n").ToList();
        }

        public (int RowIndex, int ColumnIndex) FindStartingCharPosition(List<string> lines)
        {
            int rowIndex = lines.FindIndex(x => x.Contains('@'));
            if (rowIndex == -1)
                return (-1, -1);    

            int columnIndex = lines[rowIndex].IndexOf('@');

            return (rowIndex, columnIndex);
        }

        public bool CheckCharacterAndProcessIt(char item, int rowIndex, int columnIndex, StringBuilder characters, List<Tuple<int, int>> usedLettersCords){

            if (char.IsLetter(item))
            {
                var letterCords = new Tuple<int, int>(rowIndex, columnIndex);
                if (usedLettersCords.Contains(letterCords))
                    return false;

                usedLettersCords.Add(letterCords);
                characters.Append(item);
            }

            return true;
        }
    }
}
