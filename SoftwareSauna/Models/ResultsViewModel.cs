﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareSauna.Models
{
    public class ResultsViewModel
    {
        public ResultsViewModel(string asciiPath, string letters){
            AsciiPath = asciiPath;
            Letters = letters;
        }

        [Display(Name = "Ascii path:")]
        public string AsciiPath { get; set; }

        [Display(Name = "Letters:")]
        public string Letters { get; set; }
    }
}
