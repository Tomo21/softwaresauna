﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoftwareSauna.Models;

namespace SoftwareSauna.Models.Code.Interfaces
{
    public interface IParseAsciiMap
    {
        ResultsViewModel GetLettersAndPathResult(string asciimap);
    }
}
