﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using SoftwareSauna.Models.Code.Interfaces;
using SoftwareSauna.Models.Helpers.Interfaces;
using SoftwareSauna.Models.Analyzers.Interfaces;
using SoftwareSauna.Models.Analyzers;

namespace SoftwareSauna.Models.Code
{
    public class ParseAsciiMap : IParseAsciiMap
    {
        private IParseHelper ParseHelper { get; }
        private AnalyzerFactory AnalyzersFactory { get; }
        private enum NextParsingDirection { Starting, Vertical, Horizontal }

        private const string START_CHAR = "@";
        private const string END_CHAR = "x";

        private IAnalyzer downAnalyzer;
        private IAnalyzer upAnalyzer;
        private IAnalyzer leftAnalyzer;
        private IAnalyzer rightAnalyzer;

        public ParseAsciiMap(IParseHelper parseHelper, AnalyzerFactory factory)
        {
            ParseHelper = parseHelper;
            AnalyzersFactory = factory;
        }

        public ResultsViewModel GetLettersAndPathResult(string asciimap)
        {
            var lines = ParseHelper.SeparateLines(asciimap);
            var indexes = ParseHelper.FindStartingCharPosition(lines);

            if (indexes.RowIndex == -1)
                return new ResultsViewModel("Passed ascii map is missing starting character '@'!", "");

            var result = Parse(lines, indexes.RowIndex, indexes.ColumnIndex);

            return new ResultsViewModel(result.FormattedPath, result.OnlyCharacters);
        }

        private (string FormattedPath, string OnlyCharacters) Parse(List<string> lines, int rowIndex, int columnIndex)
        {
            StringBuilder path = new StringBuilder(START_CHAR), characters = new StringBuilder();
            var usedLettersCords = new List<Tuple<int, int>>();
            NextParsingDirection direction = NextParsingDirection.Starting;

            downAnalyzer = AnalyzersFactory.ExecuteCreation(AnalyzerFactoryType.DownAnalyzer);
            upAnalyzer = AnalyzersFactory.ExecuteCreation(AnalyzerFactoryType.UpAnalyzer);
            leftAnalyzer = AnalyzersFactory.ExecuteCreation(AnalyzerFactoryType.LeftAnalyzer);
            rightAnalyzer = AnalyzersFactory.ExecuteCreation(AnalyzerFactoryType.RightAnalyzer);

            while (!path.ToString().EndsWith(END_CHAR)){
                (int RowIndex, int ColumnIndex)? result = null;
                
                if(direction == NextParsingDirection.Starting || direction == NextParsingDirection.Horizontal){
                    if ((result = HorizontalParse(lines, rowIndex, columnIndex, path, characters, usedLettersCords)) != null)
                    {

                        direction = NextParsingDirection.Vertical;
                        rowIndex = result.Value.RowIndex;
                        columnIndex = result.Value.ColumnIndex;
                    }
                }
                
                if((direction == NextParsingDirection.Starting || direction == NextParsingDirection.Vertical) && result == null){
                    if ((result = VerticalParse(lines, rowIndex, columnIndex, path, characters, usedLettersCords)) != null)
                    {

                        direction = NextParsingDirection.Horizontal;
                        rowIndex = result.Value.RowIndex;
                        columnIndex = result.Value.ColumnIndex;
                    }
                }
               
                if(result == null)
                    return ("Error unrecognized format of the map!", "");

            }

            return (path.ToString(), characters.Replace(END_CHAR, "").ToString());
        }

        private (int RowIndex, int ColumnIndex)? VerticalParse(List<string> lines, int rowIndex, int columnIndex, StringBuilder path, StringBuilder characters, List<Tuple<int, int>> usedLettersCords)
        {
            (int RowIndex, int ColumnIndex)? result;
            if ((result = downAnalyzer.StartAnalyzing(lines, rowIndex, columnIndex, path, characters, usedLettersCords, ParseHelper)) == null){
                result = upAnalyzer.StartAnalyzing(lines, rowIndex, columnIndex, path, characters, usedLettersCords, ParseHelper);
            }

            return result;
        }

        private (int RowIndex, int ColumnIndex)? HorizontalParse(List<string> lines, int rowIndex, int columnIndex, StringBuilder path, StringBuilder characters, List<Tuple<int, int>> usedLettersCords)
        {
            (int RowIndex, int ColumnIndex)? result;
            if ((result = leftAnalyzer.StartAnalyzing(lines, rowIndex, columnIndex, path, characters, usedLettersCords, ParseHelper)) == null){
                result = rightAnalyzer.StartAnalyzing(lines, rowIndex, columnIndex, path, characters, usedLettersCords, ParseHelper);
            }

            return result;
        }
    }
}
