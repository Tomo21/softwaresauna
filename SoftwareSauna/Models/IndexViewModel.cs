﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SoftwareSauna.Models
{
    public class IndexViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Required field!")]
        [Display(Name = "Enter Ascii Map:")]
        public string AsciiMap { get; set; }
    }
}
