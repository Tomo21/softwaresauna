﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SoftwareSauna.Models;
using SoftwareSauna.Models.Code.Interfaces;
using SoftwareSauna.Models.Code;

namespace SoftwareSauna.Controllers
{
    public class HomeController : Controller
    {
        public IParseAsciiMap ParseAsciiMap { get; }

        public HomeController(IParseAsciiMap parseAsciiMap){
            ParseAsciiMap = parseAsciiMap;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ProcessData(IndexViewModel mdl){
            if(ModelState.IsValid){
                
                var result = await Task.Run(() => ParseAsciiMap.GetLettersAndPathResult(mdl.AsciiMap));
                return View("Results", result);
            }
            
            return null;
        }
    }
}